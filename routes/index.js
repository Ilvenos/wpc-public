var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();
var busboy = require('connect-busboy');
var uuid = require('node-uuid');
var AWS = require('aws-sdk');
AWS.config.region = 'eu-central-1';
var s3bucket = new AWS.S3({params: {Bucket: '166625-mgamrat'}});
var sqs = new AWS.SQS(); 

//upload part
var bucket_address = 'https://s3.eu-central-1.amazonaws.com/166625-mgamrat';

router.use(busboy());
    router.get('/upload', function (req, res) {
        
    });
    router.post('/upload', function (req, res) {
        
        
        req.pipe(req.busboy);
        
        req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
              if (!filename) {
                // If filename is not truthy it means there's no file
                return;
              }
              // Create the initial array containing the stream's chunks
              file.fileRead = [];

              file.on('data', function(chunk) {
                // Push chunks into the fileRead array
                this.fileRead.push(chunk);
              });

              file.on('error', function(err) {
                console.log('Error while buffering the stream: ', err);
              });

              file.on('end', function() {
                var uid = uuid.v4();
                var newFileName = uid+'_____'+filename;
                
                // Concat the chunks into a Buffer
                var finalBuffer = Buffer.concat(this.fileRead);
                 
                var file = {
                  buffer: finalBuffer,
                  size: finalBuffer.length,
                  filename: newFileName,
                  mimetype: mimetype
                };
                sendToAws(file, newFileName, res);
              });
            
            
            
        });
        
        function sendToAws(file, fileName, res){
            var s3 = new AWS.S3(); 
            s3.createBucket({Bucket: '166625-mgamrat'}, function() {
             var params = {Bucket: '166625-mgamrat', Key: fileName, Body: file.buffer, ACL: 'public-read'};
             s3.putObject(params, function(err, data) {
                 if (err) {
                     console.log(err) 
                 }    
                 else {
                     var image = {};
                     image.name = fileName;
                     image.link = bucket_address+'/'+fileName;
                     res.send(JSON.stringify(image));
                 }     
              });
           });
            console.log(fileName);
//            var bucket_name = 'bucketName';
//            s3 = boto3.resource('s3');
//            var bucket = s3.Bucket(bucket_name);
//            bucket.put_object(Key=fileName, Body=file, ACL='public-read');

        }  
    });
router.post('/create-album', function (req, res) {
        var obj = {};
	console.log('body: ' + JSON.stringify(req.body));
        sendToQueue();
        function sendToQueue(){
           var params = {
            MessageBody: JSON.stringify(req.body),
            QueueUrl: 'https://sqs.eu-central-1.amazonaws.com/881078108084/166625-mgamrat'
          };
          sqs.sendMessage(params, function(err, data) {
            if (err){
                console.logError(err, err.stack); // an error occurred
            }  
            else  {
                 console.log(data);   
                 res.send(req.body);// successful response
            }       
          });   
        }
        
        
});
router.use(busboy());
//end upload part
router.get('/', function(req, res) {
    res.render('index', { user : req.user, error : req.flash('error')});
});

router.get('/register', function(req, res) {
    res.render('register', { });
});

router.post('/register', function(req, res, next) {
    Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
        if (err) {
          return res.render('register', { error : err.message });
        }

        passport.authenticate('local')(req, res, function () {
            req.session.save(function (err) {
                if (err) {
                    return next(err);
                }
                res.redirect('/');
            });
        });
    });
});


router.get('/login', function(req, res) {
    res.render('login', { user : req.user, error : req.flash('error')});
});

router.post('/', passport.authenticate('local', { failureRedirect: '/', failureFlash: true }), function(req, res, next) {
    req.session.save(function (err) {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});


router.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }), function(req, res, next) {
    req.session.save(function (err) {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});

router.get('/logout', function(req, res, next) {
    req.logout();
    req.session.save(function (err) {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});

router.get('/ping', function(req, res){
    res.status(200).send("pong!");
});


module.exports = router;
