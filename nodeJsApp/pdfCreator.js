var mu = require('mu2');
var pdf = require('html-pdf');
var uuid = require('node-uuid');
var AWS = require('aws-sdk');
AWS.config.region = 'eu-central-1';
var s3bucket = new AWS.S3({params: {Bucket: '166625-mgamrat'}});
//var sqs = new AWS.SQS();
var bucket_address = 'https://s3.eu-central-1.amazonaws.com/166625-mgamrat';
mu.root = '/home/wpc-public/templates';
var pdfOptions = { format: 'A4', "orientation": "landscape" };
//queue
//https://REGION_ENDPOINT/queue.amazonaws.com/YOUR_ACCOUNT_NUMBER/YOUR_QUEUE_NAME
var SQSWorker = require('sqs-worker')

//var queueUrl = getQueueUrl();
var options = { url: 'https://sqs.eu-central-1.amazonaws.com/881078108084/166625-mgamrat' };
var queue = new SQSWorker(options, worker);

var workerDone;
//email
var userEmail = '';
var ses = new AWS.SES({region: 'us-west-2'});

var fromEmail = 's166625@wizard.uek.krakow.pl';
//endemail
function worker(notifi, done) {
    try {
        var message = {};
        //{"email":"t","images":[{"name":"584c29fa-9604-4dbc-aa07-5261eb17b436_____12654647_974282002607537_7798179861389974677_n-758x758 - Copy.jpg","link":"https://s3.eu-central-1.amazonaws.com/166625-mgamrat/584c29fa-9604-4dbc-aa07-5261eb17b436_____12654647_974282002607537_7798179861389974677_n-758x758 - Copy.jpg"}]}
        if (typeof notifi !== "undefined"){
            message = JSON.parse(notifi);
            if (message.email && message.images) {
                console.log('receive message');
                workerDone = done;
                console.log('passed function');
                userEmail = message.email;
                var pdfData = {};
                pdfData.images = message.images;
                console.log(pdfData);
                createPDF(pdfData);
            }
        } 
    } catch (err) {
        throw err;
    }  
}


function createPDF(data){
//    var data = {
//    images: [
//        { link: "http://www.fpoimg.com/2048x2048" },
//        { link: "http://www.fpoimg.com/1024x2048" },
//        { link: "http://www.fpoimg.com/600x600" },
//        { link: "http://www.fpoimg.com/800x600" },
//        { link: "http://www.fpoimg.com/2048x2048" },
//        { link: "http://www.fpoimg.com/1024x2048" },
//        { link: "http://www.fpoimg.com/600x600" },
//        { link: "http://www.fpoimg.com/800x600" },
//        { link: "http://www.fpoimg.com/2048x2048" },
//        { link: "http://www.fpoimg.com/1024x2048" },
//        { link: "http://www.fpoimg.com/600x600" },
//        { link: "http://www.fpoimg.com/800x600" }
//    ]
//};
    var html = '';
    var stream = mu.compileAndRender('pdf-template.html', data);
    stream.on('data',function(buffer){
      var part = buffer.toString();
      html += part;
    });
    stream.on('end',function(){
     renderPDF(html);
    });  
}

function renderPDF(html){
//  pdf.create(html, options).toFile('../tmp/test.pdf', function(err, res) {
//    if (err) return console.log(err);
//    console.log(res); // { filename: '/app/businesscard.pdf' } 
//  });  
    pdf.create(html, pdfOptions).toBuffer(function(err, buffer){
        var fileName = uuid.v4() + '.pdf';
        var file = {};
        file.buffer = buffer;
        sendToAws(file, fileName);
    });
}

function sendToAws(file, fileName){
    var s3 = new AWS.S3(); 
    s3.createBucket({Bucket: '166625-mgamrat'}, function() {
     var params = {Bucket: '166625-mgamrat', Key: fileName, Body: file.buffer, ACL: 'public-read'};
     s3.putObject(params, function(err, data) {
         if (err) {
             console.log(err);
         }    
         else {
                sendEmail(fileName);
         }     
      });
    });
}
function sendEmail(pdfName){
    var body = 'Twój album dostępny jest pod adresem: ' + bucket_address + '/' + pdfName;
    var subject = 'Album ' + pdfName;
    ses.sendEmail( { 
        Source:fromEmail,
        Destination:{
            ToAddresses: [
                userEmail
            ]
        },
        Message:{
            Subject: {
                Data: subject,
                Charset: 'utf-8'
            },
            Body: {
                Text: {
                    Data: body,
                    Charset: 'utf-8'
                },
                Html: {
                    Data: body,
                    Charset: 'utf-8'
                }
            }
        },
        ReplyToAddresses:[
            fromEmail
        ],
        ReturnPath:fromEmail
}
, function(err, data) {
    if(err) throw err;
        console.log('Email sent:');
        console.log(data);
        // Call `done` when you are done processing a message. 
        // If everything went successfully and you don't want to see it any more, 
        // set the second parameter to `true`. 
        workerDone(null,true);
 });
}
function getQueueUrl() {
  var params = {
  QueueName: "166625-mgamrat", 
  QueueOwnerAWSAccountId: "881078108084"
 };
 sqs.getQueueUrl(params, function(err, data) {
   if (err) {
       console.log(err, err.stack);
   } else {
        console.log(data);           // successful response
        options.url = data.QueueUrl;
        startWorker();
   }
          
   /*
   data = {
    QueueUrl: "https://queue.amazonaws.com/12345678910/MyQueue"
   }
   */
 }); 
}
